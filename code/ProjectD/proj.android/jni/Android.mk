LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

$(call import-add-path,$(LOCAL_PATH)/../../cocos2d)
$(call import-add-path,$(LOCAL_PATH)/../../cocos2d/external)
$(call import-add-path,$(LOCAL_PATH)/../../cocos2d/cocos)

LOCAL_MODULE := cocos2dcpp_shared

LOCAL_MODULE_FILENAME := libcocos2dcpp


# add kihol, for find all cpp in Classes
CLASSES_DIRECTORY := $(LOCAL_PATH)/../../Classes

SOURCE_FILES := $(shell find $(CLASSES_DIRECTORY) -name *.cpp)
SOURCE_FILES := $(sort $(SOURCE_FILES))
SOURCE_FILES := $(subst $(LOCAL_PATH)/,,$(SOURCE_FILES))

# $(info SOURCE_FILES:$(SOURCE_FILES))

LOCAL_SRC_FILES := hellocpp/main.cpp \
                   $(SOURCE_FILES) \
                   # ../../Classes/app_main/AppDelegate.cpp \
                   # ../../Classes/scene/GameScene.cpp

LOCAL_C_INCLUDES := $(CLASSES_DIRECTORY)/app_main \
                    $(CLASSES_DIRECTORY)/scene \

LOCAL_STATIC_LIBRARIES := cocos2dx_static

include $(BUILD_SHARED_LIBRARY)

$(call import-module,.)
