//
//  GameScene.h
//  ProjectD
//
//  Created by Kihol on 2014. 12. 6..
//
//

#ifndef __ProjectD__GameScene__
#define __ProjectD__GameScene__

#include <cocos2d.h>
USING_NS_CC;

class GameScene : public Scene
{
public:
	static GameScene* create();
	
private:
	GameScene();
	virtual ~GameScene();
	
	virtual bool	init();
	
	virtual void	onEnter();
	virtual void	onExit();
	
};

#endif /* defined(__ProjectD__GameScene__) */
