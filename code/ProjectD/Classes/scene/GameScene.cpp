//
//  GameScene.cpp
//  ProjectD
//
//  Created by Kihol on 2014. 12. 6..
//
//

#include "GameScene.h"

static const Color4B	COLOR_SKY			= Color4B(76, 190, 236, 255);

GameScene* GameScene::create()
{
	auto scene = new GameScene();
	scene->init();
	scene->autorelease();
	return scene;
}

GameScene::GameScene()
{
	
}
GameScene::~GameScene()
{
	
}

bool GameScene::init()
{
	Size screenSize = Director::getInstance()->getVisibleSize();
	
	// 하늘색으로 배경 적용
	auto sky = LayerColor::create(COLOR_SKY);
	sky->setContentSize(screenSize);
	this->addChild(sky);
	
	// Test Label
	auto testLabel = Label::createWithTTF("Hangmy~~(나눔고딕추가볼드)", "fonts/NanumGothicExtraBold.ttf", 60);
	testLabel->setAnchorPoint(Vec2(0.5, 1));
	testLabel->setPosition(Vec2(screenSize.width/2, screenSize.height-50));
	testLabel->setColor(Color3B::GREEN);
	this->addChild(testLabel);
	
	// Test Label2
	auto testLabel2 = Label::createWithTTF("대양이 경일이 여긴 처음이지 어서와~(나눔고딕볼드)", "fonts/NanumGothicBold.ttf", 60);
	testLabel2->setAnchorPoint(Vec2(0.5, 1));
	testLabel2->setPosition(Vec2(screenSize.width/2, screenSize.height-250));
	testLabel2->setColor(Color3B::BLACK);
	this->addChild(testLabel2);
	
	// Test Label3
	auto testLabel3 = Label::createWithTTF("ANG(cocosFont Can't Korean)", "fonts/Marker Felt.ttf", 60);
	testLabel3->setAnchorPoint(Vec2(0.5, 1));
	testLabel3->setPosition(Vec2(screenSize.width/2, screenSize.height-400));
	testLabel3->setColor(Color3B::RED);
	this->addChild(testLabel3);
	
	// Test image
	auto leaf = Sprite::create("images/leaf_1.png");
	leaf->setPosition(Vec2(screenSize.width/2, screenSize.height/4));
	this->addChild(leaf);
	
	// rotate forever
	leaf->runAction(RepeatForever::create(RotateBy::create(0.5f, 180)));
	
	return true;
}

void GameScene::onEnter()
{
	Scene::onEnter();
}
void GameScene::onExit()
{
	Scene::onExit();
}


